<?php

/**
 *
 * api для работы с пользователями
 * в рамках задания важен только метод parsecsvAction
 * можно было бы реализовать форму заливки файла с фронта, но такой задачи не стояло
 * для наглядности есть метод listAction
 */
class UserController extends BaseController

{

    /**
     * "/user/parsecsv"  fill user table from csv file
     *
     * @param string $path
     */
    public function parsecsvAction($path = PROJECT_ROOT_PATH . '/Файл для теста в CSV.csv')
    {
        $data = [];

        $arrQueryStringParams = $this->getQueryStringParams();
        if (isset($arrQueryStringParams['path']) && $arrQueryStringParams['path']) {

            $path = $arrQueryStringParams['path'];

        }

        if (!file_exists($path)) {
            throw new Exception('File not exists');
        }
        $csvFile = file($path);
        foreach ($csvFile as $line) {
            $data[]    = str_getcsv($line);
            $userModel = new User();
            $userModel->insert(str_getcsv($line));
        }
    }


    /**
     * "/user/list" Endpoint - Get list of users
     */

    public function listAction()

    {

        $strErrorDesc = '';
        $this->checkGetMethod();
        try {

            $userModel = new User();

            $intLimit = 10;

            if (isset($arrQueryStringParams['limit']) && $arrQueryStringParams['limit']) {

                $intLimit = $arrQueryStringParams['limit'];

            }

            $arrUsers = $userModel->getUsers($intLimit);

            $responseData = json_encode($arrUsers);

        } catch (Error $e) {

            $strErrorDesc = $e->getMessage() . 'Something went wrong! Please contact support.';

            $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';

        }

        // send output

        if (!$strErrorDesc) {

            $this->sendOutput(

                $responseData,

                array('Content-Type: application/json', 'HTTP/1.1 200 OK')

            );

        } else {

            $this->sendOutput(json_encode(array('error' => $strErrorDesc)),

                array('Content-Type: application/json', $strErrorHeader)

            );

        }

    }

}
