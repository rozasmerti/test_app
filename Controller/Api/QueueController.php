<?php

/**
 * api для работы с рассылкой
 *
 */
class QueueController extends BaseController

{

    /**
     * первичное создание
     *
     * @param string $title
     * @param string $text
     *
     * @return void
     * @throws Exception
     */
    public function createAction($title = 'Тестовая рассылка', $text = 'Текст рассылки')
    {
        $this->checkGetMethod();

        $arrQueryStringParams = $this->getQueryStringParams();
        if (isset($arrQueryStringParams['title']) && $arrQueryStringParams['title']) {

            $title = $arrQueryStringParams['title'];

        }

        if (isset($arrQueryStringParams['text']) && $arrQueryStringParams['text']) {

            $title = $arrQueryStringParams['text'];

        }

        $queueModel = new QueueList();
        $queueModel->insert($title, $text);

        $users  = new User();
        $result = $users->select("SELECT * FROM users");

        $queueStatus = new QueueStatus();
        $queueStatus->insertNew($queueModel->getId(), $result);

        $this->sendOutput(

            json_encode(['status' => 'ok']),

            array('Content-Type: application/json', 'HTTP/1.1 200 OK')

        );
    }

    /**
     * непосредственная обработка и смена статуса
     *
     * @param int $queueId
     *
     * @return void
     */
    public function sendAction($queueId = 1)
    {
        $arrQueryStringParams = $this->getQueryStringParams();
        if (isset($arrQueryStringParams['$queueId']) && $arrQueryStringParams['$queueId']) {

            $title = $arrQueryStringParams['$queueId'];

        }

        $queueStatus = new QueueStatus();
        $queueStatus->sendQueue($queueId);

        $this->sendOutput(

            json_encode(['status' => 'ok']),

            array('Content-Type: application/json', 'HTTP/1.1 200 OK')

        );
    }
}
