<?php

/**
 * Base connect to database
 */
class Database

{

    protected $connection = null;

    public function __construct()
    {
        try {
            $connection = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);

            // set the PDO error mode to exception
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

    }

    public function select($query = "", $params = [])
    {

        try {

            $stmt = $this->connection->prepare($query);

            $stmt->execute();
            $result = $stmt;

            $stmt->close();

            return $result;

        } catch (Exception $e) {

            throw new Exception($e->getMessage());

        }

        return false;

    }

    public function getId()
    {
        return $this->connection->lastInsertId();
    }



}

