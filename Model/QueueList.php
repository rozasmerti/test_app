<?php

require_once PROJECT_ROOT_PATH . "/Model/Database.php";

class QueueList extends Database

{

    /**
     * create database
     *
     * @return void
     */
    public function migrate()

    {

        $sql = "CREATE TABLE `test_csv`.`queue_list` ( `id` INT NOT NULL AUTO_INCREMENT , `title` TEXT NOT NULL , `text` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->connection->query($sql);

    }

    /**
     * inset new queue
     *
     * @return void
     */
    function insert($title, $text)
    {
        $this->connection->exec("INSERT INTO queue_list (title, text)
                                 VALUES ('$title', '$text')");
    }

}

