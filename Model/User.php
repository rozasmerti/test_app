<?php

require_once PROJECT_ROOT_PATH . "/Model/Database.php";

class User extends Database

{

    /**
     * create database
     *
     * @return void
     */
    public function migrate()

    {

        $sql = "CREATE TABLE `test_csv`.`users` ( `id` INT NOT NULL AUTO_INCREMENT , `name` TEXT NOT NULL , `number` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->connection->query($sql);

    }

    /**
     * inset user
     *
     * @return void
     */
    function insert($param)
    {
        foreach ($param as $key => $var) {
            $param[$key] = trim($var);
        }
        $this->connection->exec("INSERT INTO users (name, number)
  VALUES ('$param[1]', '$param[0]')");
    }

    public function getUsers($limit = 10)
    {

        return $this->select("SELECT * FROM users ORDER BY id DESC LIMIT ?", ["i", $limit]);

    }

}
