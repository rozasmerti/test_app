<?php

require_once PROJECT_ROOT_PATH . "/Model/Database.php";

/**
 *
 */
class QueueStatus extends Database

{

    /**
     * create database
     *
     * @return void
     */
    public function migrate()

    {

        $sql = "CREATE TABLE `test_csv`.`queue_status` ( `id` INT NOT NULL AUTO_INCREMENT , `queue_id` INT NOT NULL , `user_id` INT NOT NULL , `user_number` VARCHAR(50) NOT NULL,  `status` BOOLEAN NOT NULL DEFAULT FALSE , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->connection->query($sql);

    }

    /**
     * inset new queue users
     *
     * @return void
     */
    function insertNew($queuId, $result)
    {
        foreach ($result->fetchAll() as $k => $v) {
            $id     = $v["id"];
            $number = $v["number"];
            $this->connection->exec("INSERT INTO queue_status (queue_id, user_id, user_number, status)
                                    VALUES ($queuId, $id, $number, 0)");
        }
    }


    /**
     * @param int $queueId
     *
     * @return void
     */
    function sendQueue($queueId)
    {
        $result = $this->select("SELECT * FROM queue_status LEFT JOIN queue_list ON queue_list.id = queue_status.queue_id WHERE queue_id = $queueId AND status = 0");
        foreach ($result->fetchAll() as $k => $v) {
            $id = $v["id"];
            if ($this->sendSms($v['user_number'], $v['title'], $v['text']) === true) {
                $sql = "UPDATE queue_status SET status =1 WHERE id=" . $id;

                $query = $this->connection->prepare($sql);
                $query->execute();
            }
        }
    }


    /**
     * fake method for send sms
     *
     * @param $userNumber
     * @param $title
     * @param $text
     *
     * @return true
     */
    function sendSms($userNumber, $title, $text)
    {
        return true;
    }
}
