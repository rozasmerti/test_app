<?php

require __DIR__ . "/config/app.php";

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$uri = explode('/', $uri);

if ((!isset($uri[2]) && in_array($uri[2], array('user', 'queue'))) || !isset($uri[3])) {

    header("HTTP/1.1 404 Not Found");

    exit();

}

// точка входа для апи, конечно роуты лучше бы развести покрасивее, а не так топорно
if ($uri[2] == 'user') {
    require PROJECT_ROOT_PATH . "/Controller/Api/UserController.php";

    $objFeedController = new UserController();

    $strMethodName = $uri[3] . 'Action';

    $objFeedController->{$strMethodName}();
}

if ($uri[2] == 'queue') {
    require PROJECT_ROOT_PATH . "/Controller/Api/QueueController.php";

    $objFeedController = new QueueController();

    $strMethodName = $uri[3] . 'Action';

    $objFeedController->{$strMethodName}();
}

