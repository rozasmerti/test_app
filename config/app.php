<?php

define("PROJECT_ROOT_PATH", __DIR__ . "/../");

// include db configuration file

require_once PROJECT_ROOT_PATH . "/config/db.php";

// include the base controller file

require_once PROJECT_ROOT_PATH . "/Controller/Api/BaseController.php";

// include the use model file

require_once PROJECT_ROOT_PATH . "/Model/User.php";
require_once PROJECT_ROOT_PATH . "/Model/QueueList.php";
require_once PROJECT_ROOT_PATH . "/Model/QueueStatus.php";
